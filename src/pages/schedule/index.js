import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Button, FlatList, StyleSheet } from 'react-native';

export default function ScheduleScreen() {
  const [bookTitle, setBookTitle] = useState('');
  const [scheduledDate, setScheduledDate] = useState('');
  const [bookScheduleList, setBookScheduleList] = useState([]);

  useEffect(() => {
    getBookScheduleList();
  }, []);

  const getBookScheduleList = async () => {
    try {
      const storedBookScheduleList = await AsyncStorage.getItem('bookScheduleList');
      if (storedBookScheduleList !== null) {
        setBookScheduleList(JSON.parse(storedBookScheduleList));
      }
    } catch (error) {
      console.error('Error getting book schedule list: ', error);
    }
  };

  const saveSchedule = async () => {
    try {
      const newSchedule = { bookTitle, scheduledDate };
      const updatedBookScheduleList = [...bookScheduleList, newSchedule];
      await AsyncStorage.setItem('bookScheduleList', JSON.stringify(updatedBookScheduleList));
      setBookScheduleList(updatedBookScheduleList);
      setBookTitle('');
      setScheduledDate('');
      alert('Schedule saved successfully!');
    } catch (error) {
      console.error('Error saving schedule: ', error);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Schedule Screen</Text>
      <TextInput
        style={styles.input}
        placeholder="Enter book title"
        onChangeText={text => setBookTitle(text)}
        value={bookTitle}
      />
      <TextInput
        style={styles.input}
        placeholder="Enter scheduled date"
        onChangeText={text => setScheduledDate(text)}
        value={scheduledDate}
      />
      <Button
        title="Save Schedule"
        onPress={saveSchedule}
      />
      <Text style={styles.subtitle}>Book Schedule List:</Text>
      <FlatList
        data={bookScheduleList}
        renderItem={({ item }) => (
          <View style={styles.item}>
            <Text style={styles.textItem}>Title: {item.bookTitle}</Text>
            <Text style={styles.textItem}>Scheduled Date: {item.scheduledDate}</Text>
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingTop: 50
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginVertical: 10,
    paddingHorizontal: 10,
    width: '100%',
    borderRadius: 8
  },
  item: {
    marginBottom: 10,
    backgroundColor: 'purple',
    borderRadius: 8,
    padding: 8,
  },
  textItem:{
    fontSize: 24,
  }
});
