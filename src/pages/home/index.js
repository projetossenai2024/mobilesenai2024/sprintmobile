import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Platform, Image, FlatList } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicons from 'react-native-vector-icons/Ionicons';

const HomeScreen = () => {
  const [categories, setCategories] = useState([
    { title: 'Ciências' },
    { title: 'Artes' },
    { title: 'Design' },
    { title: 'Literatura' },
    { title: 'História' },
    { title: 'Biografias' },
    { title: 'Tecnologia' },
    { title: 'Negócios' },
    { title: 'Autoajuda' },
    { title: 'Infantil' },
  ]);

  const [popularBooks, setPopularBooks] = useState([
    { title: 'Just My Type', author: 'Allan Garfield', image: require('../../../assets/images/book-1.jpg') },
    { title: 'Milk and Honey', author: 'Rupi Kaur', image: require('../../../assets/images/book-2.jpg') },
    { title: 'Sapiens', author: 'Yuval Noah Harari', image: require('../../../assets/images/book-3.jpg') },
    // ... adicione mais livros aqui
  ]);

  const [borrowedBooks, setBorrowedBooks] = useState([
    {
      title: 'Dominicana',
      author: '',
      returnDate: '09.09.2023',
      image: require('../../../assets/images/book-1.jpg'), // Substitua por sua imagem
    },
    {
      title: 'Still like designer',
      author: 'Alvin Kleon',
      returnDate: '11.09.2023',
      image: require('../../../assets/images/book-1.jpg'), // Substitua por sua imagem
    },
    {
      title: 'O Pequeno Príncipe',
      author: 'Antoine de Saint-Exupéry',
      returnDate: '15.09.2023',
      image: require('../../../assets/images/book-1.jpg'), // Substitua por sua imagem
    },
  ]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.logo}>Logo da Biblioteca</Text>
        <TouchableOpacity style={styles.profileButton}>
          <Ionicons name="person-circle-outline" size={30} color="#007BFF" />
        </TouchableOpacity>
      </View>

      <View style={styles.searchBar}>
        <Ionicons name="search" size={20} color="#ccc" style={{ marginRight: 10 }} />
        <TextInput style={styles.searchInput} placeholder="Pesquisar livros" />
      </View>

      <View style={styles.content}>
        <Text style={styles.sectionTitle}>Categorias</Text>
        <View style={styles.categoriesContainer}>
          {categories.map((category, index) => (
            <Text key={index} style={styles.category}>{category.title}</Text>
          ))}
        </View>

        <Text style={styles.sectionTitle}>Populares</Text>
        <FlatList
          data={popularBooks}
          renderItem={({ item }) => (
            <View style={styles.bookCard}>
              <Image
                source={item.image}
                style={styles.bookImage}
              />
              <View style={styles.bookInfo}>
                <Text style={styles.bookTitle}>{item.title}</Text>
                <Text style={styles.bookAuthor}>{item.author}</Text>
              </View>
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
        />

        <Text style={styles.sectionTitle}>Emprestados</Text>
        <FlatList
          data={borrowedBooks}
          renderItem={({ item }) => (
            <View style={styles.bookCard}>
              <Image
                source={item.image}
                style={styles.bookImage}
              />
              <View style={styles.bookInfo}>
                <Text style={styles.bookTitle}>{item.title}</Text>
                <Text style={styles.bookAuthor}>{item.author}</Text>
                <Text style={styles.borrowedDate}>Data de Devolução: {item.returnDate}</Text>
              </View>
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    paddingTop: Platform.OS === 'ios' ? 60 : 40,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  logo: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#333', // Cor um pouco mais escura
  },
  profileButton: {
    // Adicione estilos conforme necessário
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#e0e0e0',
    borderRadius: 8,
    paddingHorizontal: 10,
    marginBottom: 15,
  },
  searchInput: {
    flex: 1,
    paddingVertical: 8, // Aumenta um pouco
  },
  content: {
    flex: 1,
  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: '600',
    marginBottom: 10,
    color: '#333',
  },
  categoriesContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 15,
  },
  category: {
    fontSize: 16,
    marginBottom: 5,
    color: '#555',
    marginRight: 10,
  },
  bookCard: {
    backgroundColor: '#f8f8f8',
    marginBottom: 15,
    padding: 15,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.15,
    shadowRadius: 2,
  },
  bookImage: {
    width: 70,
    height: 100,
    resizeMode: 'cover',
    borderRadius: 5,
    marginRight: 15,
  },
  bookInfo: {
    flex: 1,
  },
  bookTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333'
  },
  bookAuthor: {
    fontSize: 14,
    color: '#555',
  },
  borrowedDate: {
    fontSize: 12,
    color: '#777',
  },
});

export default HomeScreen;
